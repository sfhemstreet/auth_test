import App from "next/app";
import { NextComponentType, NextPageContext } from "next";
import { Auth0Provider } from "@auth0/auth0-react";
import Router from "next/router";

interface MyAppProps extends App {
  Component: NextComponentType<NextPageContext, any, {}>;
  pageProps: any;
}

const onRedirectCallback = (appState: any) => {
  Router.replace(appState?.returnTo || '/');
}

const MyApp = ({ Component, pageProps }: MyAppProps) => {
  return (
    <Auth0Provider
      domain="dev-yrz9o58l.us.auth0.com"
      clientId="fEHT6VNdDgNANcbhgW62Jeh7lH2ZU6RP"
      redirectUri={typeof window !== 'undefined' ? window.location.origin : "http://localhost:3000"}
      onRedirectCallback={onRedirectCallback}
      audience="https://dev-yrz9o58l.us.auth0.com/api/v2/"
      // scope="read:current_user update:current_user_metadata"
    >
        <Component {...pageProps} />
    </Auth0Provider>

  );
};

export default MyApp;
