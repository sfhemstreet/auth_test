import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

import Router from "next/router";
import { authApi } from "../utils/authApi";

export default function Profile() {
  const {
    user,
    isAuthenticated,
    isLoading,
    error,
    getAccessTokenSilently,
  } = useAuth0();
  const [data, setData] = useState<any>(null);

  useEffect(() => {
    (async function() {

      const accessToken = await getAccessTokenSilently({
        audience: 'https://dev-yrz9o58l.us.auth0.com/api/v2/',
        // scope: 'read:current_user'
      })

      const { apiData, isApiLoading, apiError } = await authApi("/api/hello", accessToken);

      if (apiError) {
        console.log(apiError);
      }

      setData(apiData);

    }());
    
  }, []);

  if (!isAuthenticated) {
    return <div>not auth</div>;
  }

  if (isLoading) {
    return <div>loading</div>;
  }

  if (error) {
    console.log(error);
    return <div>error</div>;
  }

  return (
    <div>
      <img src={user.picture} alt={user.name} />
      <h2>{user.name}</h2>
      <p>{user.email}</p>
      <h3>User Metadata</h3>
      {data ? <pre>{JSON.stringify(data, null, 2)}</pre> : "No data "}
    </div>
  );
}
