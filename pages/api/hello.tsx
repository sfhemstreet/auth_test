// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from "next";
import { verifyAuthHeader } from "../../jwtVerify";


export default async (req: NextApiRequest, res: NextApiResponse) => {

  const authHeaders = req.headers.authorization;

  if (!authHeaders) {
    console.log("No Auth Headers")
    res.statusCode = 401
    res.json({ error: 'No Auth' })
  } else {
    const result = await verifyAuthHeader(authHeaders);
    console.log(result)
    res.json({ sub: result});
  }
}
