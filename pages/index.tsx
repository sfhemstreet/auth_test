import Head from 'next/head'
import { useEffect } from 'react'
import Link from "next/link";
import { useAuth0 } from "@auth0/auth0-react";


export default function Home() {

  const {user, isAuthenticated, isLoading, loginWithRedirect, logout} = useAuth0();

  if (isLoading) {
    return <div>loading</div>
  }

  if (isAuthenticated) {
    return (
      <div>
      Hi

      <div>
       hello {user.name}
      </div>

      <div>
        <button onClick={() => logout({ returnTo: window.location.origin })}>Log Out</button>
        <Link href="/profile" >
          <a>Go To Profile</a>
        </Link>
      </div>
    </div>
    )
  }

  return (
    <div>
      Hi

      <div>
        
      </div>

      <div>
        <button onClick={() => loginWithRedirect()}>Log In</button>
        
      </div>
    </div>
  )
}
