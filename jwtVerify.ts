import jwt from "jsonwebtoken";
import jwksRsa from "jwks-rsa";
import { promisify } from "util";

const jwksClient = jwksRsa({
  jwksUri: `https://dev-yrz9o58l.us.auth0.com/.well-known/jwks.json`,
  cache: true,
  rateLimit: true,
  jwksRequestsPerMinute: 5,
});

const verifyOptions: jwt.VerifyOptions = {
  audience: "https://dev-yrz9o58l.us.auth0.com/api/v2/",
  issuer: "https://dev-yrz9o58l.us.auth0.com/",
  algorithms: ["RS256"],
};

async function getKey(header: jwt.JwtHeader): Promise<string> {
  const getPubKey = promisify(jwksClient.getSigningKey);
  const key = await getPubKey(header.kid || "");
  const pubKey = key.getPublicKey();
  return pubKey;
}

/**
 * VerifyAuthHeader checks to see if auth header contains a valid token,
 * and returns the sub field of the token, or an empty string if invalid.
 *
 * @param authHeader
 */
export async function verifyAuthHeader(authHeader: string): Promise<string> {
  // Remove 'Bearer ' and get the JWT by itself.
  const token = authHeader.split(" ")[1];

  if (!token) {
    console.log(
      "\n🔍 verifyAuthHeader: No token present after removing 'Bearer ' 🔍\n"
    );
    return "";
  }

  try {
    // Decoding the token before verifying it makes it possible
    // to get the public key asynchronously without callbacks.
    const decodedToken = jwt.decode(token, { complete: true });

    console.log("\nDecoded Token\n", decodedToken);

    if (!decodedToken) {
      console.log("\n🔍 verifyAuthHeader: decodedToken is null 🔍\n");
      return "";
    }

    // Use decoded token's header to acquire key. Requires typescript clownery.
    const pubKey = await getKey(
      (decodedToken as { [key: string]: any }).header
    );
    // Now check if the token is good without any callbacks.
    const verifiedToken = jwt.verify(token, pubKey, verifyOptions);

    console.log("\nVerified Token\n", verifiedToken);

    const subject = (verifiedToken as { [key: string]: string }).sub || "";
    return subject;
  } catch (err) {
    console.log("\n🚨 verifyAuthHeader: Error -> ", err);
    return "";
  }
}
