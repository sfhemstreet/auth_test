type FetchOptions = {
  method?: "GET" | "POST" | "DELETE";
  headers?: Headers | string[][] | Record<string, string>;
};

export async function authApi(
  url: string,
  accessToken: string,
  options?: FetchOptions,
) {
  try {
    const res = await fetch(url, {
      ...options,
      headers: {
        ...options?.headers,
        // Add the Authorization header with token 
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const data = await res.json();

    return {
      apiData: data,
      apiError: null,
      isApiLoading: false,
    };
  } catch (error) {
    console.log(`useApi - Error: ${error}`);

    return {
      apiData: null,
      apiError: error,
      isApiLoading: false,
    };
  }
}
