import { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

type ApiOptions = {
  audience: string;
  scope: string;
  fetchOptions?: {
    method?: "GET" | "POST" | "DELETE";
    headers?: Headers | string[][] | Record<string, string> | undefined;
  };
};

export const useApi = (url: string, options: ApiOptions) => {
  const { getAccessTokenSilently } = useAuth0();
  const [state, setState] = useState<{
    apiError: Error | null;
    isApiLoading: boolean;
    apiData: any;
  }>({
    apiError: null,
    isApiLoading: true,
    apiData: null,
  });
  const [refreshIndex, setRefreshIndex] = useState(0);

  useEffect(() => {
    (async () => {
      try {
        const { audience, scope, fetchOptions } = options;
        const accessToken = await getAccessTokenSilently({ audience, scope });

        console.log(`useApi - Access Token: ${accessToken}`)

        const res = await fetch(url, {
          ...fetchOptions,
          headers: {
            ...fetchOptions?.headers,
            // Add the Authorization header to the existing headers
            Authorization: `Bearer ${accessToken}`,
          },
        });
        const data = await res.json()

        setState({
          ...state,
          apiData: data,
          apiError: null,
          isApiLoading: false,
        });
      } catch (error) {

        console.log(`useApi - Error: ${error}`)

        setState({
          ...state,
          apiError: error,
          isApiLoading: false,
        });
      }
    })();
  }, []);

  return {
    ...state,
    refresh: () => setRefreshIndex(refreshIndex + 1),
  };
};
